package com.grayscale.annotation;

import java.lang.annotation.*;

/**
 * @author liuli
 * 注解
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Inherited
public @interface GrayScaleFunction {
    /**指定的灰度方法 不配置的默是原来的方法名+GrayScale */
    String grayScaleMethod() default "";
}
