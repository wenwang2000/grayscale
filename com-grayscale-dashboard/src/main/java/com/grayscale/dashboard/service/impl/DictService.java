package com.grayscale.dashboard.service.impl;

import com.grayscale.core.enums.GrayScaleTypeEnum;
import com.grayscale.dashboard.vo.SysDictData;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

/**
 * @author liuli
 * @date 2022/2/23
 */
@Service("dict")
public class DictService {

    /**
     * 根据字典类型查询字典数据信息
     *
     * @param dictType 字典类型
     * @return 参数键值
     */
    public List<SysDictData> getType(String dictType)
    {
        switch (dictType) {
            case "GRAYSCALE_RULE":
                return getGrayScaleRule();


            default:break;

        }
      return null;
    }

    private List<SysDictData> getGrayScaleRule() {
        List<SysDictData> sysDictDataList = new ArrayList<>();
        for (GrayScaleTypeEnum grayScaleTypeEnum: GrayScaleTypeEnum.values()) {
            SysDictData sysDictData = new SysDictData();
            sysDictData.setDictValue(grayScaleTypeEnum.getType());
            sysDictData.setDictLabel(grayScaleTypeEnum.getType()+"【"+grayScaleTypeEnum.getDescription()+"】");
            sysDictData.setExtData(String.valueOf(grayScaleTypeEnum.getNeedParameterKey()));
            sysDictData.setExtData2(grayScaleTypeEnum.getExample());
            sysDictDataList.add(sysDictData);
        }
        return sysDictDataList;
    }
}
