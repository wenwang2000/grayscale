package com.grayscale.dashboard.service;

import com.grayscale.dashboard.vo.ApplicationVo;
import com.grayscale.dashboard.vo.ServerInfoStatisticsVo;
import com.grayscale.dashboard.web.page.PageDomain;
import com.grayscale.dashboard.web.page.TableDataInfo;

/**
 * @author liuli
 * @date 2022/2/21
 */
public interface ApplicationService {

    /**
     * 获取应用列表
     * @param applicationVo
     * @return
     */
    TableDataInfo getApplication(ApplicationVo applicationVo, PageDomain pageDomain);

    /***
     * 系统状态开关
      * @param applicationVo
     * @return
     */
    int changeSwitch(ApplicationVo applicationVo);

    /***
     * 统计信息
     * @return
     */
    ServerInfoStatisticsVo getServerInfoStatistics();
}
