package com.grayscale.dashboard.service;

import com.grayscale.dashboard.vo.FunctionVo;
import com.grayscale.dashboard.web.page.PageDomain;
import com.grayscale.dashboard.web.page.TableDataInfo;

/**
 * @author liuli
 * @date 2022/2/22
 */
public interface FunctionService {

    TableDataInfo getApplication(FunctionVo interfaceVo, PageDomain pageDomain);

    int changeSwitch(FunctionVo interfaceVo);
}
