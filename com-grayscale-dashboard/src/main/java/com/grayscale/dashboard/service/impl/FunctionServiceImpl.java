package com.grayscale.dashboard.service.impl;

import com.github.pagehelper.PageInfo;
import com.grayscale.core.config.GrayScaleZookeeper;
import com.grayscale.core.constant.GrayScaleConstants;
import com.grayscale.core.util.GrayScalePathBuilder;
import com.grayscale.dashboard.service.FunctionService;
import com.grayscale.dashboard.util.StringUtils;
import com.grayscale.dashboard.vo.FunctionVo;
import com.grayscale.dashboard.web.base.BaseService;
import com.grayscale.dashboard.web.page.PageDomain;
import com.grayscale.dashboard.web.page.TableDataInfo;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author liuli
 * @date 2022/2/22
 */
@Service
public class FunctionServiceImpl extends BaseService implements FunctionService {
    @Resource
    GrayScaleZookeeper grayScaleZookeeper;
    @Override
    public TableDataInfo getApplication(FunctionVo interfaceVo, PageDomain pageDomain) {
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(0);
        if (StringUtils.isBlank(interfaceVo.getAppCode())){
            rspData.setRows(new ArrayList<>());
            rspData.setTotal(0);
            return rspData;
        }
        List<String> originalList= grayScaleZookeeper.getChildrenPath(GrayScalePathBuilder.buildGrayScaleRootPath()+"/"+interfaceVo.getAppCode()+ GrayScaleConstants.SERVER);
        //没有数据就不处理了
        if (originalList == null || originalList.size() <=0){
            rspData.setRows(new ArrayList<>());
            rspData.setTotal(0);
            return rspData;
        }
        //查询条件过滤
        List<String> list = new ArrayList<>();
        if (StringUtils.isNotBlank(interfaceVo.getInterfaceCode())){
            for (int i = 0; i < originalList.size(); i++) {
                if (originalList.get(i).contains(interfaceVo.getInterfaceCode())){
                    list.add(originalList.get(i));
                }
            }
            if (list == null || list.size() <=0){
                rspData.setRows(list);
                rspData.setTotal(0);
                return rspData;
            }
        }else{
            list = originalList;
        }
        int fromIndex=(pageDomain.getPageNum()-1)*pageDomain.getPageSize();
        int toIndex =fromIndex+pageDomain.getPageSize();
        if (fromIndex>list.size()){
            fromIndex =0;
            toIndex = 0;
        }else if(list.size() <= toIndex){
            toIndex = list.size();
        }
        List<String> pageList = list.subList(fromIndex,toIndex);
        List<FunctionVo> applicationVoList = new ArrayList<>();
        for (int i = 0; i < pageList.size(); i++) {
            FunctionVo vo = new FunctionVo();
            vo.setAppCode(interfaceVo.getAppCode());
            vo.setInterfaceCode(pageList.get(i));
            String pathData = grayScaleZookeeper.getPathData(GrayScalePathBuilder.buildGrayScaleRootPath()+"/"+interfaceVo.getAppCode()+GrayScaleConstants.SERVER+"/"+pageList.get(i));
            if (StringUtils.isNotBlank(pathData)){
                vo.setGrayscaleSwitch(true);
            }else{
                vo.setGrayscaleSwitch(false);
            }
            vo.setCount(grayScaleZookeeper.getCounter(GrayScalePathBuilder.buildGrayScaleRootPath()+"/"+interfaceVo.getAppCode()+GrayScaleConstants.SERVER+"/"+pageList.get(i)+GrayScaleConstants.count));
            applicationVoList.add(vo);
        }
        rspData.setRows(applicationVoList);
        rspData.setTotal(new PageInfo(list).getTotal());
        return rspData;
    }

    @Override
    public int changeSwitch(FunctionVo interfaceVo) {
        String path =GrayScalePathBuilder.buildGrayScaleRootPath()+"/"+interfaceVo.getAppCode()+GrayScaleConstants.SERVER+"/"+interfaceVo.getInterfaceCode();
        if (interfaceVo.getGrayscaleSwitch()){
            grayScaleZookeeper.setPathData(path,"1");
        }else{
            grayScaleZookeeper.delPathData(path);
        }
        return 1;
    }
}
