package com.grayscale.dashboard.service;

import com.grayscale.dashboard.vo.RuleVo;
import com.grayscale.dashboard.web.page.PageDomain;
import com.grayscale.dashboard.web.page.TableDataInfo;

/**
 * @author liuli
 * @date 2022/2/22
 */
public interface RuleService {
    /***
     * 获取系统规则
     * @param ruleVo
     * @param pageDomain
     * @return
     */
    TableDataInfo getRule(RuleVo ruleVo, PageDomain pageDomain);

    /**
     * 规则个数
     * @return
     */
    int getRulesNumber();
}
