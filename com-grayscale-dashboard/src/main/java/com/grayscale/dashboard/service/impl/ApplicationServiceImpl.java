package com.grayscale.dashboard.service.impl;

import com.github.pagehelper.PageInfo;
import com.grayscale.core.config.GrayScaleZookeeper;
import com.grayscale.core.constant.GrayScaleConstants;
import com.grayscale.core.util.GrayScalePathBuilder;
import com.grayscale.dashboard.service.ApplicationService;
import com.grayscale.dashboard.service.FunctionService;
import com.grayscale.dashboard.service.NodeService;
import com.grayscale.dashboard.util.StringUtils;
import com.grayscale.dashboard.vo.ApplicationVo;
import com.grayscale.dashboard.vo.ServerInfoStatisticsVo;
import com.grayscale.dashboard.web.base.BaseService;
import com.grayscale.dashboard.web.page.PageDomain;
import com.grayscale.dashboard.web.page.TableDataInfo;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author liuli
 * @date 2022/2/21
 */
@Service
public class ApplicationServiceImpl extends BaseService implements ApplicationService {

    @Resource
    GrayScaleZookeeper grayScaleZookeeper;
    @Resource
    NodeService nodeService;
    @Resource
    FunctionService functionService;
    @Override
    public TableDataInfo getApplication(ApplicationVo applicationVo, PageDomain pageDomain) {
        List<String> originalList= grayScaleZookeeper.getChildrenPath(GrayScalePathBuilder.buildGrayScaleRootPath());
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(0);
        //没有数据就不处理了
        if (originalList == null || originalList.size() <=0){
            rspData.setRows(originalList);
            rspData.setTotal(0);
            return rspData;
        }
        //查询条件过滤
        List<String> list = new ArrayList<>();
         if (StringUtils.isNotBlank(applicationVo.getAppCode())){
             for (int i = 0; i < originalList.size(); i++) {
                 if (originalList.get(i).contains(applicationVo.getAppCode())){
                     list.add(originalList.get(i));
                 }
             }
             if (list == null || list.size() <=0){
                 rspData.setRows(list);
                 rspData.setTotal(0);
                 return rspData;
             }
         }else{
             list = originalList;
         }
        int fromIndex=(pageDomain.getPageNum()-1)*pageDomain.getPageSize();
        int toIndex =fromIndex+pageDomain.getPageSize();
        if (fromIndex>list.size()){
            fromIndex =0;
            toIndex = 0;
        }else if(list.size() <= toIndex){
            toIndex = list.size();
        }
        List<String> pageList = list.subList(fromIndex,toIndex);
        List<ApplicationVo> applicationVoList = new ArrayList<>();
        for (int i = 0; i < pageList.size(); i++) {
            ApplicationVo app = new ApplicationVo();
            app.setAppCode(pageList.get(i));
            String pathData = grayScaleZookeeper.getPathData(GrayScalePathBuilder.buildGrayScaleRootPath()+"/"+pageList.get(i));
            if (StringUtils.isNotBlank(pathData)){
                app.setGrayscaleSwitch(true);
            }else{
                app.setGrayscaleSwitch(false);
            }
            applicationVoList.add(app);
        }
        rspData.setRows(applicationVoList);
        rspData.setTotal(new PageInfo(list).getTotal());
        return rspData;
    }

    @Override
    public int changeSwitch(ApplicationVo applicationVo) {
        if (applicationVo.getGrayscaleSwitch()){
            grayScaleZookeeper.setPathData(GrayScalePathBuilder.buildGrayScaleRootPath()+"/"+applicationVo.getAppCode(),"1");
        }else{
            grayScaleZookeeper.delPathData(GrayScalePathBuilder.buildGrayScaleRootPath()+"/"+applicationVo.getAppCode());
        }
        return 1;
    }

    @Override
    public ServerInfoStatisticsVo getServerInfoStatistics() {
        ServerInfoStatisticsVo serverInfoStatisticsVo = new ServerInfoStatisticsVo();
        serverInfoStatisticsVo.setServerNumber(0);
        serverInfoStatisticsVo.setNodeNumber(0);
        serverInfoStatisticsVo.setFunctionNumber(0);
        //获取服务
        List<String> serverList= grayScaleZookeeper.getChildrenPath(GrayScalePathBuilder.buildGrayScaleRootPath());
        if (serverList != null){
            serverInfoStatisticsVo.setServerNumber(serverList.size());
            for (int i = 0; i < serverList.size(); i++) {
                //获取在线节点
                List<String> nodeList = nodeService.getNode(serverList.get(i));
                if (nodeList !=null){
                    serverInfoStatisticsVo.setNodeNumber(serverInfoStatisticsVo.getNodeNumber()+nodeList.size());
                }
              //获取方法
                List<String> functionList= grayScaleZookeeper.getChildrenPath(GrayScalePathBuilder.buildGrayScaleRootPath()+"/"+serverList.get(i)+ GrayScaleConstants.SERVER);
                if (functionList !=null){
                    serverInfoStatisticsVo.setFunctionNumber(serverInfoStatisticsVo.getFunctionNumber()+functionList.size());
                }
            }
        }
        return serverInfoStatisticsVo;
    }
}
