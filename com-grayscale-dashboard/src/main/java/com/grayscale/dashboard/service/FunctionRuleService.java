package com.grayscale.dashboard.service;

import com.grayscale.core.entity.InterfaceRuleVo;
import com.grayscale.dashboard.web.base.AjaxResult;
import com.grayscale.dashboard.web.page.PageDomain;
import com.grayscale.dashboard.web.page.TableDataInfo;

/**
 * @author liuli
 * @date 2022/2/22
 */
public interface FunctionRuleService {

    TableDataInfo getInterfaceRule(InterfaceRuleVo interfaceRuleVo, PageDomain pageDomain);

    /**
     * 获取接口参数
     * @param appCode
     * @param interfaceCode
     * @return
     */
    String getParameter(String appCode, String interfaceCode);

    /**
     * 添加
     * @param interfaceRuleVo
     * @return
     */
    AjaxResult addRule(InterfaceRuleVo interfaceRuleVo);

    /***
     * 根据ID获取规则编码
     * @param appCode
     * @param interfaceCode
     * @param id
     * @return
     */
    InterfaceRuleVo getInterfaceRuleById(String appCode,String interfaceCode, Long id);

    /***
     *
     * @param interfaceRuleVo
     * @return
     */
    AjaxResult remove(InterfaceRuleVo interfaceRuleVo);

    /***
     * 修改规则
     * @param interfaceRuleVo
     * @return
     */
    AjaxResult editRule(InterfaceRuleVo interfaceRuleVo);
}
