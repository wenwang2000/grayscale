package com.grayscale.dashboard.service.impl;

import com.grayscale.core.config.GrayScaleZookeeper;
import com.grayscale.core.constant.GrayScaleConstants;
import com.grayscale.core.util.GrayScalePathBuilder;
import com.grayscale.dashboard.service.NodeService;
import com.grayscale.dashboard.web.base.BaseService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

/**
 * @author liuli
 * @date 2022/2/22
 */
@Service
public class NodeServiceImpl extends BaseService implements NodeService {
    @Resource
    GrayScaleZookeeper grayScaleZookeeper;
    @Override
    public List<String> getNode(String appCode) {
        return grayScaleZookeeper.getChildrenPath(GrayScalePathBuilder.buildGrayScaleRootPath()+"/"+appCode+ GrayScaleConstants.NODE);
    }
}
