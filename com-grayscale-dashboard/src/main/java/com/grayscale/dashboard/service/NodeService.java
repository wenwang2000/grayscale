package com.grayscale.dashboard.service;

import java.util.List;

/**
 * @author liuli
 * @date 2022/2/22
 */
public interface NodeService {
    /**
     * 节点名称
     * @param appCode
     * @return
     */
    List<String> getNode(String appCode);
}
