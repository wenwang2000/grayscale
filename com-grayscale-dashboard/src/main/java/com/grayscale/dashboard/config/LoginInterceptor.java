package com.grayscale.dashboard.config;

import com.grayscale.dashboard.constant.Constants;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * 登录拦截器
 * @author liuli
 * @date 2022/2/18
 */
public class LoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        Object user = request.getSession().getAttribute(Constants.LOGIN_USER);
        if (user == null || user.equals(""))  {
            PrintWriter out = response.getWriter();
            out.println("<html>");
            out.println("<script>");
            out.println("window.open ('"+request.getContextPath()+"/login','_top')");
            out.println("</script>");
            out.println("</html>");
            return false;
        }
        return true;
    }
}