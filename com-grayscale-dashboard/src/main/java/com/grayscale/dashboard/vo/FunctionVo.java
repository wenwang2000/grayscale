package com.grayscale.dashboard.vo;

/**
 * @author liuli
 * @date 2022/2/22
 */
public class FunctionVo {

    /**系统编码*/
    String appCode;

    /**接口编码*/
    String interfaceCode;

    /**灰度开关*/
    Boolean grayscaleSwitch;

    /**调用次数*/
    Long count;

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public String getInterfaceCode() {
        return interfaceCode;
    }

    public void setInterfaceCode(String interfaceCode) {
        this.interfaceCode = interfaceCode;
    }

    public Boolean getGrayscaleSwitch() {
        return grayscaleSwitch;
    }

    public void setGrayscaleSwitch(Boolean grayscaleSwitch) {
        this.grayscaleSwitch = grayscaleSwitch;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
