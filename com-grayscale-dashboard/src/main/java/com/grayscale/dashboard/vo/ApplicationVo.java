package com.grayscale.dashboard.vo;

/**
 * 系统洗洗
 * @author liuli
 * @date 2022/2/21
 */
public class ApplicationVo {
    /**系统编码*/
    String appCode;

    /**灰度开关*/
    Boolean grayscaleSwitch;

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public Boolean getGrayscaleSwitch() {
        return grayscaleSwitch;
    }

    public void setGrayscaleSwitch(Boolean grayscaleSwitch) {
        this.grayscaleSwitch = grayscaleSwitch;
    }
}
