package com.grayscale.dashboard.vo;

/**
 * 规则列表
 * @author liuli
 * @date 2022/2/22
 */
public class RuleVo {

    /**规则类型*/
    String type;
    /**业务处理类*/
    String dealClass;
    /**规则描述*/
    String description;
    /**是否需要参数KEY*/
    Boolean needParameterKey;
    /**参数个数*/
    int parameterNum;
    /**例子*/
    String example;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDealClass() {
        return dealClass;
    }

    public void setDealClass(String dealClass) {
        this.dealClass = dealClass;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getNeedParameterKey() {
        return needParameterKey;
    }

    public void setNeedParameterKey(Boolean needParameterKey) {
        this.needParameterKey = needParameterKey;
    }

    public int getParameterNum() {
        return parameterNum;
    }

    public void setParameterNum(int parameterNum) {
        this.parameterNum = parameterNum;
    }

    public String getExample() {
        return example;
    }

    public void setExample(String example) {
        this.example = example;
    }
}
