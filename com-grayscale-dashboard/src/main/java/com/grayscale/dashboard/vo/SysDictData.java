package com.grayscale.dashboard.vo;


import com.grayscale.dashboard.util.StringUtils;

/**
 * 字典数据表 sys_dict_data
 * 
 * @author liuli
 */
public class SysDictData
{
    private static final long serialVersionUID = 1L;

    /** 字典标签 */

    private String dictLabel;

    /** 字典键值 */

    private String dictValue;


    /** 是否默认（Y是 N否） */

    private String isDefault;

    /** 状态（0正常 1停用） */

    private String status;

    /**扩展参数*/
    private String extData;

    /**扩展参数2*/
    private String extData2;

    private String  listClass;

    public String getListClass() {
        return listClass;
    }

    public void setListClass(String listClass) {
        if (StringUtils.isBlank(listClass)){
            listClass ="success";
        }
        this.listClass = listClass;
    }

    public String getDictLabel() {
        return dictLabel;
    }

    public void setDictLabel(String dictLabel) {
        this.dictLabel = dictLabel;
    }

    public String getDictValue() {
        return dictValue;
    }

    public void setDictValue(String dictValue) {
        this.dictValue = dictValue;
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getExtData() {
        return extData;
    }

    public void setExtData(String extData) {
        this.extData = extData;
    }

    public String getExtData2() {
        return extData2;
    }

    public void setExtData2(String extData2) {
        this.extData2 = extData2;
    }
}
