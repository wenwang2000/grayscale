package com.grayscale.dashboard.vo;

/**
 * 服务信息统计
 * @author liuli
 * @date 2022/3/2
 */
public class ServerInfoStatisticsVo {
    /**服务个数*/
    int serverNumber;
    /**在线节点个数*/
    int nodeNumber;
    /**灰度方法个数*/
    int functionNumber;

    public int getServerNumber() {
        return serverNumber;
    }

    public void setServerNumber(int serverNumber) {
        this.serverNumber = serverNumber;
    }

    public int getNodeNumber() {
        return nodeNumber;
    }

    public void setNodeNumber(int nodeNumber) {
        this.nodeNumber = nodeNumber;
    }

    public int getFunctionNumber() {
        return functionNumber;
    }

    public void setFunctionNumber(int functionNumber) {
        this.functionNumber = functionNumber;
    }
}
