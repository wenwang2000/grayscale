package com.grayscale.dashboard.vo;

/**
 * @author liuli
 * @date 2022/2/21
 * 登录用户信息
 */
public class UserVo {

    /**用户名*/
    private String password;
    /**密码*/
    private String username;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
