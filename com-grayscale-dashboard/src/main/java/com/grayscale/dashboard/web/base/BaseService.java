package com.grayscale.dashboard.web.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author liuli
 * service层数据处理
 */
public class BaseService {
    protected Logger log = LoggerFactory.getLogger(this.getClass());

}
