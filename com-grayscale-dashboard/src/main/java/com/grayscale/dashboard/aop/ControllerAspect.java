package com.grayscale.dashboard.aop;
import com.grayscale.dashboard.util.ServletUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import javax.servlet.http.HttpServletRequest;

/**
 * 记录请求日志
 * @author liuli
 */
@Aspect
@Order(-1)
@Component
public class ControllerAspect {
    private  Logger log = LoggerFactory.getLogger(this.getClass());



    @Pointcut("execution(* com.grayscale.dashboard.controller..*(..)) && (@annotation(org.springframework.web.bind.annotation.RequestMapping) || @annotation(org.springframework.web.bind.annotation.GetMapping) || @annotation(org.springframework.web.bind.annotation.PostMapping)) ")
    public void controllerMethodPointcut() {
    }


    @Around("controllerMethodPointcut()")
    public Object Interceptor(ProceedingJoinPoint pjp) throws Throwable {
        long beginTime = System.currentTimeMillis();
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        String uri = request.getRequestURI();
        String paramStr = ServletUtils.getParamMapToStr(request);
        Object result = null;
        result = pjp.proceed();
        long costMs = System.currentTimeMillis() - beginTime;
        log.info("Request URI:【{}】 request params:【{}】 consuming:【{}ms】",uri,paramStr,costMs);
        return result;
    }

}
