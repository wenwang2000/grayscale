package com.grayscale.dashboard.controller;

import com.grayscale.dashboard.constant.Constants;
import com.grayscale.dashboard.service.FunctionRuleService;
import com.grayscale.core.entity.InterfaceRuleVo;
import com.grayscale.dashboard.service.NodeService;
import com.grayscale.dashboard.web.base.AjaxResult;
import com.grayscale.dashboard.web.base.BaseController;
import com.grayscale.dashboard.web.page.PageDomain;
import com.grayscale.dashboard.web.page.TableDataInfo;
import com.grayscale.dashboard.web.page.TableSupport;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 * 接口规则
 * @author liuli
 * @date 2022/2/22
 */
@Controller
@RequestMapping("/functionRule")
public class FunctionRuleController extends BaseController {

    @Resource
    FunctionRuleService functionRuleService;
    @Resource
    NodeService nodeService;

    @GetMapping()
    public String interfaceRule(ModelMap mmap, HttpServletRequest request)
    {
        String interfaceCode = null;
        String appCode = null;
        try {
            appCode = URLDecoder.decode(request.getParameter("appCode"),"utf-8");
            interfaceCode = URLDecoder.decode(request.getParameter("interfaceCode"),"utf-8");
        } catch (UnsupportedEncodingException e) {
            appCode = request.getParameter("appCode");
            interfaceCode = request.getParameter("interfaceCode");

        }
        request.getSession().setAttribute(Constants.GRAYSCALE_APPCODE,appCode);
        request.getSession().setAttribute(Constants.GRAYSCALE_INTERFACECODE,interfaceCode);
        mmap.put("appCode",appCode);
        mmap.put("interfaceCode", interfaceCode);
        String parameter = functionRuleService.getParameter(appCode,interfaceCode);
        mmap.put("parameter",parameter);
        List<String> nodes = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            nodes.add("192.186.10.225:808"+i);
        }
        mmap.put("nodes",nodeService.getNode(appCode));
        return "functionRule";
    }


    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(InterfaceRuleVo interfaceRuleVo,HttpServletRequest request)
    {
        interfaceRuleVo.setAppCode(String.valueOf(request.getSession().getAttribute(Constants.GRAYSCALE_APPCODE)));
        interfaceRuleVo.setInterfaceCode(String.valueOf(request.getSession().getAttribute(Constants.GRAYSCALE_INTERFACECODE)));
        PageDomain pageDomain = TableSupport.buildPageRequest();
        return functionRuleService.getInterfaceRule(interfaceRuleVo,pageDomain);
    }

    @GetMapping("/add/{appCode}/{interfaceCode}")
    public String add(@PathVariable("appCode") String appCode,@PathVariable("interfaceCode") String interfaceCode,ModelMap mmap)
    {
        try {
           appCode = URLDecoder.decode(appCode,"utf-8");
           interfaceCode = URLDecoder.decode(interfaceCode,"utf-8");
        } catch (UnsupportedEncodingException e) {
           appCode = appCode;
           interfaceCode = interfaceCode;
        }
        mmap.put("appCode",appCode);
        mmap.put("interfaceCode", interfaceCode);
        return "/functionRule/add";
    }

    @GetMapping("/edit/{appCode}/{interfaceCode}/{id}")
    public String edit(@PathVariable("appCode") String appCode,@PathVariable("interfaceCode") String interfaceCode,@PathVariable("id") Long id,ModelMap mmap)
    {

        mmap.put("appCode",appCode);
        mmap.put("interfaceCode", interfaceCode);
        mmap.put("id",id);
        InterfaceRuleVo interfaceRule = functionRuleService.getInterfaceRuleById(appCode,interfaceCode,id);
        mmap.put("functionRule", interfaceRule);
        return "/functionRule/edit";
    }

    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(InterfaceRuleVo interfaceRuleVo)
    {

        return functionRuleService.addRule(interfaceRuleVo);
    }
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(InterfaceRuleVo interfaceRuleVo)
    {

        return functionRuleService.editRule(interfaceRuleVo);
    }

    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(InterfaceRuleVo interfaceRuleVo)
    {
        return functionRuleService.remove(interfaceRuleVo);
    }


}
