package com.grayscale.dashboard.controller;


import com.grayscale.dashboard.constant.Constants;
import com.grayscale.dashboard.service.FunctionService;
import com.grayscale.dashboard.vo.FunctionVo;
import com.grayscale.dashboard.web.base.AjaxResult;
import com.grayscale.dashboard.web.base.BaseController;
import com.grayscale.dashboard.web.page.PageDomain;
import com.grayscale.dashboard.web.page.TableDataInfo;
import com.grayscale.dashboard.web.page.TableSupport;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * 切量方法
 * @author liuli
 * @date 2022/2/9
 *
 */
@Controller
@RequestMapping("/function")
public class FunctionController extends BaseController {

    @Resource
    FunctionService functionService;

    @GetMapping()
    public String function(ModelMap mmap, HttpServletRequest request)
    {
        try {
            request.getSession().setAttribute(Constants.GRAYSCALE_APPCODE,URLDecoder.decode(request.getParameter("appCode"),"utf-8"));
        } catch (UnsupportedEncodingException e) {
            request.getSession().setAttribute(Constants.GRAYSCALE_APPCODE,request.getParameter("appCode"));
        };
        return  "/function";
    }


    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(FunctionVo interfaceVo, HttpServletRequest request)
    {
        interfaceVo.setAppCode(String.valueOf(request.getSession().getAttribute(Constants.GRAYSCALE_APPCODE)));
        PageDomain pageDomain = TableSupport.buildPageRequest();
        return functionService.getApplication(interfaceVo,pageDomain);
    }

    @PostMapping("/changeSwitch")
    @ResponseBody
    public AjaxResult changeSwitch(FunctionVo interfaceVo)
    {
        return toAjax(functionService.changeSwitch(interfaceVo));
    }

}
