package com.grayscale.dashboard.controller;

import com.grayscale.dashboard.vo.UserVo;
import com.grayscale.dashboard.web.base.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.servlet.http.HttpServletRequest;


/**
 * 用户信息
 * @author liuli
 * @date 2022/2/21
 */
@Controller
@RequestMapping("/user")
public class UserInfoController extends BaseController {

    @GetMapping("/profile")
    public String profile(ModelMap mmap, HttpServletRequest request) {
        UserVo userVo = getSysUser(request);
        mmap.put("user",userVo);
        return "profile";
    }
}
