package com.grayscale.dashboard.controller;

import com.grayscale.dashboard.service.RuleService;
import com.grayscale.dashboard.vo.RuleVo;
import com.grayscale.dashboard.web.base.BaseController;
import com.grayscale.dashboard.web.page.PageDomain;
import com.grayscale.dashboard.web.page.TableDataInfo;
import com.grayscale.dashboard.web.page.TableSupport;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;

/**
 * @author liuli
 * @date 2022/2/22
 */
@Controller
@RequestMapping("/rule")
public class RuleController extends BaseController {

    @Resource
    RuleService ruleService;

    @GetMapping()
    public String rule(ModelMap mmap)
    {
        return  "/rule";
    }


    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(RuleVo ruleVo)
    {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        return ruleService.getRule(ruleVo,pageDomain);
    }

}
