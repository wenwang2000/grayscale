package com.grayscale.dashboard.controller;

import com.grayscale.dashboard.vo.UserVo;
import com.grayscale.dashboard.web.base.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import javax.servlet.http.HttpServletRequest;


/**
 * 系统首页
 * @author liuli
 * @date 2022/2/16
 */
@Controller
public class IndexController extends BaseController {


    @GetMapping("/index")
    public String index(ModelMap mmap, HttpServletRequest request) {
        UserVo userVo = getSysUser(request);
        if (userVo != null){
            mmap.put("user",userVo);
        }
        return "index";
    }
}
