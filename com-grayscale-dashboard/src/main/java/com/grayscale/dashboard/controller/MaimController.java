package com.grayscale.dashboard.controller;

import com.grayscale.dashboard.service.ApplicationService;
import com.grayscale.dashboard.service.RuleService;
import com.grayscale.dashboard.vo.ServerInfoStatisticsVo;
import com.grayscale.dashboard.web.base.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
/**
 * 系统主页
 * @author liuli
 * @date 2022/2/21
 */
@Controller
public class MaimController  extends BaseController {

    @Resource
    RuleService ruleService;
    @Resource
    ApplicationService applicationService;

    @GetMapping("/main")
    public String main(ModelMap mmap, HttpServletRequest request) {
        ServerInfoStatisticsVo serverInfoStatisticsVo = applicationService.getServerInfoStatistics();
        mmap.put("serverNumber",serverInfoStatisticsVo.getServerNumber());
        mmap.put("nodeNumber",serverInfoStatisticsVo.getNodeNumber());
        mmap.put("functionNumber",serverInfoStatisticsVo.getFunctionNumber());
        mmap.put("rulesNumber",ruleService.getRulesNumber());
        return "main";
    }
}
