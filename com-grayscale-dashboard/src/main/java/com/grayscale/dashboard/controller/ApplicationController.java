package com.grayscale.dashboard.controller;


import com.grayscale.dashboard.service.ApplicationService;
import com.grayscale.dashboard.vo.ApplicationVo;
import com.grayscale.dashboard.web.base.AjaxResult;
import com.grayscale.dashboard.web.base.BaseController;
import com.grayscale.dashboard.web.page.PageDomain;
import com.grayscale.dashboard.web.page.TableDataInfo;
import com.grayscale.dashboard.web.page.TableSupport;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;

/**
 * @author liuli
 * @date 2022/2/9
 * 页面导航
 */
@Controller
@RequestMapping("/application")
public class ApplicationController extends BaseController {

    @Resource
    ApplicationService applicationService;

    @GetMapping()
    public String application(ModelMap mmap)
    {
        return  "/application";
    }


    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ApplicationVo applicationVo)
    {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        return applicationService.getApplication(applicationVo,pageDomain);
    }

    @PostMapping("/changeSwitch")
    @ResponseBody
    public AjaxResult changeSwitch(ApplicationVo applicationVo)
    {
        return toAjax(applicationService.changeSwitch(applicationVo));
    }

}
