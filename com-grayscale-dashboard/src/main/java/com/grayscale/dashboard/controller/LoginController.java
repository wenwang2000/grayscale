package com.grayscale.dashboard.controller;

import com.grayscale.dashboard.constant.Constants;
import com.grayscale.dashboard.vo.UserVo;
import com.grayscale.dashboard.web.base.AjaxResult;
import com.grayscale.dashboard.web.base.BaseController;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 登录、登出
 * @author liuli
 * @date 2022/2/21
 */
@Controller
public class LoginController  extends BaseController {

    @Value("${user.username}")
    private String username;

    @Value("${user.password}")
    private String password;

    @GetMapping("/login")
    public String login(ModelMap mmap, HttpServletResponse response) {

        return "login";
    }


    @PostMapping("/login")
    @ResponseBody
    public AjaxResult ajaxLogin(String username, String password, HttpServletRequest request){
        UserVo userVo = new UserVo();
        userVo.setUsername(username);
        userVo.setPassword(password);
        if (this.username.equals(username) && this.password.equals(password)){
            request.getSession().setAttribute(Constants.LOGIN_USER,userVo);
            return success();
        }else{
            return error("用户名密码错误");
        }

    }


    @GetMapping("/logout")
    public String logout(HttpSession session) {
        session.removeAttribute(Constants.LOGIN_USER);
        return "login";
    }


}
