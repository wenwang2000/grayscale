package com.grayscale.dashboard;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

/**
 * com-grayscale-dashboard-ui
 * @author liuli
 * 服务启动
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class, SecurityAutoConfiguration.class  })
public class GrayScaleConsoleApp {
	private static Logger log = LoggerFactory.getLogger(GrayScaleConsoleApp.class);
	public static void main(String[] args) {
		SpringApplication.run(GrayScaleConsoleApp.class, args);
		log.info("GrayScale Console Start Success......");
	}

}
