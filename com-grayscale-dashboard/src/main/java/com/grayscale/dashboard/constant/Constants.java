package com.grayscale.dashboard.constant;

/**
 * 通用常量信息
 * 
 * @author liuli
 */
public class Constants
{
    /**
     * UTF-8 字符集
     */
    public static final String UTF8 = "UTF-8";

    /**
     * 通用成功标识
     */
    public static final String SUCCESS = "0";

    /**
     * 通用失败标识
     */
    public static final String FAIL = "1";


    /**
     * 当前记录起始索引
     */
    public static final String PAGE_NUM = "pageNum";

    /**
     * 每页显示记录数
     */
    public static final String PAGE_SIZE = "pageSize";

    /**
     * 排序列
     */
    public static final String ORDER_BY_COLUMN = "orderByColumn";

    /**
     * 排序的方向 "desc" 或者 "asc".
     */
    public static final String IS_ASC = "isAsc";

    /***
     * 登录用户信息
     */
    public static final String LOGIN_USER = "user";

    /***
     * 系统编码
     */
    public static final String GRAYSCALE_APPCODE = "grayscaleAppCode";
    /***
     * 接口编码
     */
    public static final String GRAYSCALE_INTERFACECODE = "grayscaleInterfaceCode";


}
