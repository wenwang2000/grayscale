package com.grayscale.core.config;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author liuli
 * @date 2022/2/9
 * 系统配置
 */
@Configuration
public class GrayScaleClientConfig {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    /**必须  用于控制台展示服务名称*/
    @Value("${spring.application.name}")
    private String appName;
    /**必须  服务唯一标识*/
    @Value("${spring.application.code}")
    private String appCode;
    /**非必须  用于区分单机部署多个服务，没有配置默认8080 */
    @Value("${server.port:8080}")
    private int serverPort;
    /**非必须  本地配置文件路径 */
    @Value("${grayscale.offlinePath:/grayscale}")
    private String offlinePath;
    /** 离线模式 开启后 只从本地获取配置，不再从zk获取，性能最大化，不能保证100% */
    @Value("${grayscale.offlineMode:false}")
    private Boolean offlineMode;
    /**是否注册应用（grayscale-dashboard不注册） 默认注册  */
    @Value("${grayscale.clientRegister:true}")
    private Boolean clientRegister;
    /**必须  zkServer地址*/
    @Value("${grayscale.zookeeper.connect-url}")
    private  String zkConnectUrl;
    /**重试间隔*/
    @Value("${grayscale.zookeeper.retrypolicy.base-sleep-time:1000}")
    private Integer baseSleepTime;
    /**重试次数*/
    @Value("${grayscale.zookeeper.retrypolicy.max-retries:3}")
    private Integer maxRetries;

    public String getAppName() {
        return appName;
    }

    public String getAppCode() {
        return appCode;
    }

    public int getServerPort() {
        return serverPort;
    }

    public String getOfflinePath() {
        return offlinePath+"/"+appCode;
    }

    public String getZkConnectUrl() {
        return zkConnectUrl;
    }

    public Boolean getClientRegister() {
        return clientRegister;
    }

    public Integer getBaseSleepTime() {
        return baseSleepTime;
    }

    public Integer getMaxRetries() {
        return maxRetries;
    }

    public Boolean getOfflineMode() {
        return offlineMode;
    }

    Boolean needRegister(){
        if (!getClientRegister()){
            log.info("系统名称和编码：{} {} 不需要注册",appCode,appName);
            return false;
        }
        if (StringUtils.isBlank(appCode) || StringUtils.isBlank(appName)) {
            log.error("系统名称和编码：{} {} 没有配置",appCode,appName);
            return false;
        }
        return true;
    };


}
