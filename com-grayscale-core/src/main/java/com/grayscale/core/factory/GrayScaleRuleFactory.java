package com.grayscale.core.factory;

import com.grayscale.core.adapt.GrayScaleRuleAdapt;
import com.grayscale.core.enums.GrayScaleTypeEnum;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author liuli
 * @date 2022/2/11
 */
public interface GrayScaleRuleFactory {
     Logger log = LoggerFactory.getLogger(GrayScaleRuleFactory.class);

     static GrayScaleRuleAdapt getDealClass(String type) {
        String dealClass = GrayScaleTypeEnum.getDealClass(type);
        if (StringUtils.isBlank(dealClass)){
            return  null;
        }
        try {
            return (GrayScaleRuleAdapt) Class.forName(dealClass).newInstance();
        } catch (Exception e) {
            log.error("创建处理类失败",e);
            return null;
        }

    }
}
