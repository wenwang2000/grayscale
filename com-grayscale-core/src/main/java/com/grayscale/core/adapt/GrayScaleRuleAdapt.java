package com.grayscale.core.adapt;

/**
 * @author liuli
 * @date 2022/2/11
 */
public interface GrayScaleRuleAdapt{

    /***
     * 获取规则结果
     * @param parameter
     *    第一个：配置的参数
     *    第二个：规则编码
     *    第三个：ip:port
     *    第四个：KEY的值(非必须)
     * @return
     */
    boolean process(String... parameter);

    /***
     * 校验参数是否正确
     * @param parameter
     * @return
     */
    boolean check(String parameter);
}
