package com.grayscale.core.constant;

/**
 * 系统常量
 * @author liuli
 * @date 2022/3/2
 */
public class GrayScaleConstants {

    /**server节点*/
    public static final  String SERVER ="/server";
    /**应用服务节点*/
    public static final  String NODE ="/node";
    /**规则节点*/
    public static final  String RULES ="/rules";
    /**参数节点*/
    public static final  String PARAMETERS ="/parameters";
    /**方法后缀*/
    public static final  String FUNCTION_SUFFIX ="GrayScale";
    /**计数器*/
    public static final  String count ="/count";
}
