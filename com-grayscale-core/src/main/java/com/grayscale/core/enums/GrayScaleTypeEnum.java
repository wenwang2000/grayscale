package com.grayscale.core.enums;

/**
 * @author liuli
 * @date 2022/2/10
 * 灰度切量规则
 */
public enum GrayScaleTypeEnum {
    /**随机开放百分比*/
    KEY_PERCENT("KEY_PERCENT", "com.grayscale.core.rule.GrayScalePercentRule","按照百分比随机切量",false,2,"切量5%参数VALUE(100,5);切量0.5%参数VALUE(1000,5);全切（100%）VALUE(100,100)"),

    /** 数值区间:  区间之内(包含、不包含边界)、区间之外(包含、不包含边界) */
    KEY_BETWEEN("KEY_BETWEEN", "com.grayscale.core.rule.GrayScaleKeyNumberInterval","KEY的值v1,v2之间(不包含v1,v2)",true,2,"入参KEY(orderPrice)参数VALUE(v1<= v2)  orderPrice>v1 && orderPrice<v2切换"),
    KEY_BETWEEN_DY("KEY_BETWEEN_DY", "com.grayscale.core.rule.GrayScaleKeyNumberInterval","KEY的值v1,v2之间(包含v1,v2)",true,2,"入参KEY(orderPrice)参数VALUE(v1<= v2)  orderPrice>=v1 && orderPrice<=v2切换"),
    KEY_OUTSIDE("KEY_OUTSIDE", "com.grayscale.core.rule.GrayScaleKeyNumberInterval","KEY的值v1,v2之外(不包含v1,v2)",true,2,"入参KEY(orderPrice)参数VALUE(v1<= v2)  orderPrice<v1 && orderPrice>v2切换"),
    KEY_OUTSIDE_DY("KEY_OUTSIDE", "com.grayscale.core.rule.GrayScaleKeyNumberInterval","KEY的值v1,v2之外(包含v1,v2)",true,2,"入参KEY(orderPrice)参数VALUE(v1<= v2)  orderPrice<-v1 && orderPrice>=v2切换"),
    /** 数值大小: 大于、大于等于、小于、小于等、等于 */
    KEY_DAYU("KEY_DAYU", "com.grayscale.core.rule.GrayScaleKeyNumberCompare","KEY的值>参数",true,1,"入参KEY（orderPrice）>VALUE（100）切换"),
    KEY_DAYUDY("KEY_DAYUDY", "com.grayscale.core.rule.GrayScaleKeyNumberCompare","KEY的值>=参数",true,1,"入参KEY（orderPrice）>=参数VALUE（100）切换"),
    KEY_XY("KEY_XY", "com.grayscale.core.rule.GrayScaleKeyNumberCompare","KEY的值<参数",true,1,"入参KEY（orderPrice）<参数VALUE（100）切换"),
    KEY_XYDY("KEY_XYDY", "com.grayscale.core.rule.GrayScaleKeyNumberCompare","KEY的值<=参数",true,1,"入参KEY（orderPrice）<=参数VALUE（100）切换"),
    KEY_DY("KEY_DY", "com.grayscale.core.rule.GrayScaleKeyNumberCompare","KEY的值=参数",true,1,"入参KEY（orderPrice）=参数VALUE（100）切换"),
    /**字符: 包含/不包含*/
    KEY_IN("KEY_IN", "com.grayscale.core.rule.GrayScaleKeyInOrNotInRule","KEY的值在参数(多个值,隔开)中",true,1,"入参KEY（userId）在参数VALUE（admin,001,test）中切换"),
    KEY_NOT_IN("KEY_NOT_IN", "com.grayscale.core.rule.GrayScaleKeyInOrNotInRule","KEY的值不在参数(多个值,隔开)中",true,1,"入参KEY（userId）不在参数VALUE（admin,001,test）中切换"),
    /**系统节点*/
    SERVER_IN("SERVER_IN", "com.grayscale.core.rule.GrayScaleServerNodeRule","参与的节点(多个值,隔开)中",false,1,"节点在 参数VALUE为（127.0.0.1:80,127.0.0.2:81） 中切换"),
    SERVER_NOT_IN("SERVER_NOT_IN", "com.grayscale.core.rule.GrayScaleServerNodeRule","不参与的节点(多个值,隔开)中",false,1,"节点不在 参数VALUE为（127.0.0.1:80,127.0.0.2:81）中切换"),
    /**时间节点*/
    BEFORE_TIME("BEFORE_TIME", "com.grayscale.core.rule.GrayScaleTimeRule","HH:mm:ss之前",false,1,"时间在 参数VALUE为（10:00:00）之前切换"),
    AFTER_TIME("AFTER_TIME", "com.grayscale.core.rule.GrayScaleTimeRule","HH:mm:ss之后",false,1,"时间在 参数VALUE为（10:00:00）之后切换"),
    BETWEEN_TIME("BETWEEN_TIME", "com.grayscale.core.rule.GrayScaleTimeIntervalRule","HH:mm:ss和HH:mm:ss之间",false,2,"时间在 参数VALUE为（10:00:00,20:00:00）之间切换"),
    OUTSIDE_TIME("OUTSIDE_TIME", "com.grayscale.core.rule.GrayScaleTimeIntervalRule","HH:mm:ss和HH:mm:ss之外",false,2,"时间在 参数VALUE为（10:00:00,20:00:00）之外切换"),
    BEFORE_DATETIME("BEFORE_DATETIME", "com.grayscale.core.rule.GrayScaleDateTimeRule","yyyy-MM-dd HH:mm:ss之前",false,1,"时间在 参数VALUE为（2022-02-02 10:00:00）之前切换"),
    AFTER_DATETIME("AFTER_DATETIME", "com.grayscale.core.rule.GrayScaleDateTimeRule","yyyy-MM-dd HH:mm:ss之后",false,1,"时间在 参数VALUE为（2022-02-02 10:00:00）之后切换"),
    BETWEEN_DATETIME("BETWEEN_DATETIME", "com.grayscale.core.rule.GrayScaleDateTimeIntervalRule","yyyy-MM-dd HH:mm:ss和yyyy-MM-dd HH:mm:ss之间",false,2,"时间在 参数VALUE为（2022-02-02 10:00:00,2022-02-02 20:00:00）之间切换"),
    OUTSIDE_DATETIME("OUTSIDE_DATETIME", "com.grayscale.core.rule.GrayScaleDateTimeIntervalRule","yyyy-MM-dd HH:mm:ss和yyyy-MM-dd HH:mm:ss之外",false,2,"时间在 参数VALUE为（2022-02-02 10:00:00,2022-02-02 20:00:00）之外切换"),




    ;
    /**规则类型*/
    String type;
    /**业务处理类*/
    String dealClass;
    /**规则描述*/
    String description;
    /**是否需要参数KEY*/
    Boolean needParameterKey;
    /**参数个数*/
    int parameterNum;
    /**例子*/
    String example;


    GrayScaleTypeEnum(String type, String dealClass, String description,Boolean needParameterKey,int parameterNum,String example) {
        this.type = type;
        this.dealClass = dealClass;
        this.description= description;
        this.needParameterKey = needParameterKey;
        this.parameterNum = parameterNum;
        this.example = example;
    }

    public static String getDealClass(String type) {
        for (GrayScaleTypeEnum grayScaleTypeEnum: GrayScaleTypeEnum.values()) {
            if (grayScaleTypeEnum.getType().equals(type)){
                return grayScaleTypeEnum.getDealClass();
            }
        }
        return null;
    }

    public static String getDescription(String type) {
        for (GrayScaleTypeEnum grayScaleTypeEnum: GrayScaleTypeEnum.values()) {
            if (grayScaleTypeEnum.getType().equals(type)){
                return grayScaleTypeEnum.getDescription();
            }
        }
        return null;
    }


    public static GrayScaleTypeEnum getByType(String type) {
        GrayScaleTypeEnum[] var1 = values();
        int var2 = var1.length;
        for(int var3 = 0; var3 < var2; ++var3) {
            GrayScaleTypeEnum grayScaleTypeEnum = var1[var3];
            if (grayScaleTypeEnum.getType().equals(type)) {
                return grayScaleTypeEnum;
            }
        }
        return null;
    }

    public String getType() {
        return type;
    }

    public String getDealClass() {
        return dealClass;
    }

    public String getDescription() {
        return description;
    }

    public Boolean getNeedParameterKey() {
        return needParameterKey;
    }

    public int getParameterNum() {
        return parameterNum;
    }

    public String getExample() {
        return example;
    }
}
