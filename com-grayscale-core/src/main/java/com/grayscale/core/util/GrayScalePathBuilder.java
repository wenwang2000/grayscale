package com.grayscale.core.util;

import com.grayscale.core.config.GrayScaleClientConfig;
import com.grayscale.core.constant.GrayScaleConstants;

/**
 * @author liuli
 * @date 2022/2/9
 */
public class GrayScalePathBuilder {
    private static final String DIRECTORY_PREFIX = "/grayscale";

    /**
     * 灰度系统根路径
     * @return
     */
    public static final String buildGrayScaleRootPath() {
        return DIRECTORY_PREFIX ;
    }
    /**
     * 应用路径
     * @param appName
     * @return
     */
    public static final String buildAppPath(String appName,String appCode) {
        return buildGrayScaleRootPath()+"/" + getApp(appName, appCode);
    }

    /***
     * 应用节点路径（临时节点）
     * 注册应用的所有节点 ：应用启动时创建，关闭时销毁
     * @param appName
     * @param localHost
     * @return
     */
    public static final String buildAppNodePath(String appName,String appCode,String localHost) {
        return buildAppPath(appName, appCode) +GrayScaleConstants.NODE+ "/" +localHost;
    }

    /**
     * 服务节点
     * @param appName
     * @param appCode
     * @return
     */
    public static final String buildAppServerPath(String appName,String appCode) {
        return buildAppPath(appName, appCode) + GrayScaleConstants.SERVER;
    }

    /**
     * 服务扩展节点
     * @param appName
     * @param appCode
     * @param grayScaleService
     * @return
     */
    public static final String buildAppServerPathExt(String appName,String appCode,String grayScaleService) {
        return buildAppServerPath(appName, appCode)+"/" + grayScaleService;
    }

    private static String getApp(String appName,String appCode){
        return "("+appName+")"+appCode;
    }

    /**
     * 服务开关
     * @param grayScaleClientConfig
     * @return
     */
    public static final String buildOfflineServerSwitchPath(GrayScaleClientConfig grayScaleClientConfig) {
        return grayScaleClientConfig.getOfflinePath()+"/server.json";
    }

    /***
     * 方法开关
     * @param grayScaleClientConfig
     * @return
     */
    public static final String buildOfflineFunctionSwitchPath(GrayScaleClientConfig grayScaleClientConfig,String functionName) {
        return grayScaleClientConfig.getOfflinePath()+"/"+functionName+".json";
    }

    /***
     * 方法参数
     * @param grayScaleClientConfig
     * @param functionName
     * @return
     */
    public static final String buildOfflineServerFunctionParametersPath(GrayScaleClientConfig grayScaleClientConfig,String functionName) {
        return grayScaleClientConfig.getOfflinePath()+"/"+functionName+"#parameters.json";
    }


}
