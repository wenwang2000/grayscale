package com.grayscale.core.rule;

import com.grayscale.core.adapt.GrayScaleRuleAdapt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Random;

/**
 * 随机开放百分比
 * @author liuli
 * @date 2022/3/3
 */
public class GrayScalePercentRule  implements GrayScaleRuleAdapt {
    private Logger log = LoggerFactory.getLogger(this.getClass());
    Integer v1 = 0;
    Integer v2 = 0;
    @Override
    public boolean process(String... parameter) {
        if (!check(parameter[0])){
            return false;
        }
        Random r = new Random();
        Integer randomNumber= r.nextInt(v1);
        if (randomNumber <=v2){
            return true;
        }
        return false;
    }

    @Override
    public boolean check(String parameter) {
        try {
            v1 = Integer.parseInt(parameter.split(",")[0]);
            v2 = Integer.parseInt(parameter.split(",")[1]);
            if(v1 >= v2){
              return true;
            }
            return false;
        } catch (Exception e) {
            log.error("parameter format error:{} {}",parameter,e);
            return false;
        }
    }
}
