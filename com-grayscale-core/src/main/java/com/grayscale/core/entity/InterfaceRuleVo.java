package com.grayscale.core.entity;

import com.grayscale.core.enums.GrayScaleTypeEnum;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;

/**
 * @author liuli
 * @date 2022/2/22
 */
public class InterfaceRuleVo {
    /**id*/
    Long id;

    /**系统编码*/
    String appCode;

    /**接口编码*/
    String interfaceCode;

    /**系统编码*/
    String ruleCode;

    /**规则KEY*/
    String ruleKey;

    /**接口编码*/
    String ruleValue;

    /**创建时间*/
    Date createdTime;

    /**更新时间*/
    Date updateTime;

    /**规则描述*/
    String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        if (StringUtils.isBlank(description)){
            description = GrayScaleTypeEnum.getDescription(this.ruleCode);
        }
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public String getInterfaceCode() {
        return interfaceCode;
    }

    public void setInterfaceCode(String interfaceCode) {
        this.interfaceCode = interfaceCode;
    }

    public String getRuleCode() {
        return ruleCode;
    }

    public void setRuleCode(String ruleCode) {
        this.ruleCode = ruleCode;
    }

    public String getRuleKey() {
        return ruleKey;
    }

    public void setRuleKey(String ruleKey) {
        this.ruleKey = ruleKey;
    }

    public String getRuleValue() {
        return ruleValue;
    }

    public void setRuleValue(String ruleValue) {
        this.ruleValue = ruleValue;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
