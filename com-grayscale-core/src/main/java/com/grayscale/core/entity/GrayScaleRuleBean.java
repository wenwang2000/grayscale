package com.grayscale.core.entity;

import com.grayscale.core.enums.GrayScaleTypeEnum;

/**
 * @author liuli
 * @date 2022/2/11
 * 灰度规则 信息
 * @see GrayScaleTypeEnum
 */

public class GrayScaleRuleBean {

    /**规则类型 参考 com.grayscale.core.constant.GrayScaleType */
    String ruleType;
    /**规则参数 多个参数用空格分开*/
    String parameter;

    public String getRuleType() {
        return ruleType;
    }

    public void setRuleType(String ruleType) {
        this.ruleType = ruleType;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }
}
