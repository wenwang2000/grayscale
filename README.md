# GrayScale
#### AB测试中的痛点
- 新功能上线，产品要求按照规则进行AB测试,但是规则随着业务的推进不断变化(上线前也无法完全确定)。例如：第一天要求产品价格>100的走A场景，第二天要求：在上午10到下午18之前的走B场景，第三天要求产品编码为（1001,1002,1003）的走A场景...... 
传统方案:修改规则后发版，这就导致开发工作量增加，频繁上线，时效性也很差。

#### 介绍

---
```shell
com-grayscale-annotation  （注解模块，快速下线是使用）
com-grayscale-client  （客户端demo 依赖 com-grayscale-annotation ）
com-grayscale-core     （核心模块依赖 com-grayscale-annotation ）
com-grayscale-dashboard （控制台，服务查看，规则配置）
```
GrayScale是一套轻量、高可用、配置友好的灰度程序管理器，内置20多个常用规则，用户可以按照业务场景快速开发自己的规则，离线模式满足高并发场景的需求。在实际业务场景中针对AB测试，开发一个B方法。并使用GrayScale的注解为A方法指定切量方法B,GrayScale会更具用户自定义的规则来选择执行A方法还是B方法 
，目前，GrayScale定义了3个维度的规则，服务全局开关、方法开关、方法规则 。

![3R](static/introduce.png)

#### 系统架构

---
![3R](static/framework-function.png)

- 提供标准规则库，用户可按照需求扩展
- 提供Dashboard，查看服务和规则、动态配置规则，自动同步到在线节点
- 高可用支持：规则本地持久化，规则变更后通过zookeeper监听机制动态刷新本地缓存，支持开启离线模式（直接本地获取规则不连接zk）
- 使用简单，快速接入，代码O侵入（只需一个注解+一个配置），支持更换jar包快速下线，无需修改业务代码

#### 技术架构
![3R](static/framework-technology.png)

#### 如何接入？

---
##### 第一步：准备zk集群
- 搭建好zk集群，GrayScale在zk集群中使用的根目录为 grayscale,接入请确保该目录没有被占用
  ![3R](static/directoryStructure.png)
##### 第二步：Dashboard下载和启动

- 指定zk地址
    - -- grayscale.zookeeper.connect-url 指定zookeeper地址.

```shell
java -jar com-grayscale-dashboard-1.0.jar grayscale.zookeeper.connect-url=127.0.0.1:2181,127.0.0.1:2182,127.0.0.1:2183
```
- 配置参数详解
```shell
user.username 登录名（默认：admin）
user.password: 密码（默认：123456）
grayscale.clientRegister 是否注册服务（false）控制台不注册
grayscale.zookeeper.connect-url zk集群地址（默认：127.0.0.1:2181,127.0.0.1:2182,127.0.0.1:2183）
```
  
- 启动后访问地址：
    - http://localhost:8888/index.html

- 功能预览
  

登录
![3R](static/dashboard/login.png)
首页
![3R](static/dashboard/index.png)
服务列表
![3R](static/dashboard/server.png)
接口服务列表
![3R](static/dashboard/function.png)
接口服务规则
![3R](static/dashboard/rules.png)
接口服务规则配置
![3R](static/dashboard/configRule.png)
规则列表
![3R](static/dashboard/rule.png)

##### 第二步：服务接入

- 引入maven支持：打包com-grayscale-core (上传到自己的私服，可根据项目情况调整依赖jar版本)
```xml
  <dependency>
  <groupId>com.grayscale.core</groupId>
  <artifactId>com-grayscale-core</artifactId>
  <version>1.0</version>
  </dependency>
```
- 参数配置
```shell
# 必须参数
server.port  服务端口 (默认8080)
spring.application.name 服务编码
spring.application.code 服务名称
grayscale.zookeeper.connect-url zk地址
grayscale.offlinePath 本地配置信息路劲（默认/grayscale/${spring.application.code}）
grayscale.offlineMode 是否开启离线模式（默认false）
```
-- 配置切量方法
 ```java
/**
 * TestController中调用sFunction1，按照GrayScale配置规则在满足条件的情况下切换到sFunction1GrayScale
 * 这2个方法的形参必须一样
 * @author liuli
 * @date 2022/2/9
 */
@Service
public class Server1Impl implements Server1 {

  @Override
  /**
   * 如不指定 grayScaleMethod 默认：该方法名+GrayScale
   * @GrayScaleFunction(grayScaleMethod="sFunction1GrayScale")
   * */
  @GrayScaleFunction
  public void sFunction1(String productCode, String userId, double price, PamsVo pamsVo) {
    System.out.println("sFunction1=不切量");
  }

  @Override
  public void sFunction1GrayScale(String productCode, String userId, double price, PamsVo pamsVo){
    System.out.println("sFunction1GrayScale=切量");
  }

}

  ```
##### 第三步：实例
场景一 入参：userId 在 【liuli,test,admin】走切量，配置如下：
![3R](static/demo/config1.png)
case1：userId=liuli (走切量)
![3R](static/demo/demo1-1.png)
case2：userId=xiaoming (不走切量)
![3R](static/demo/demo1-2.png)
场景二 入参：pamsVo.price  <10 走切量，配置如下：
![3R](static/demo/config2.png)
case1：pamsVo.price=8 (走切量)
![3R](static/demo/demo2-1.png)
case2：pamsVo.price=12 (不走切量)
![3R](static/demo/demo2-2.png)
##### 服务下线
- 服务快速下线： 
  
  一、把core替换成annotation 无需修改业务代码，摘除grayscale 
  
  二、关闭grayscale.zookeeper.connect-url（如项目不再需要zk）等配置

```xml
  <dependency>
  <groupId>com.grayscale.core</groupId>
  <artifactId>com-grayscale-annotation</artifactId>
  <version>1.0</version>
  </dependency>
```

#### JOIN ME

![3R](static/liuli.png)


