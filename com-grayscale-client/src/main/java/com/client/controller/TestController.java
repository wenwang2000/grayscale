package com.client.controller;


import com.client.service.Server1;
import com.client.vo.PamsVo;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 功能测试
 * @author liuli
 * @date 2022/2/9
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @Resource
    Server1 server1;

    @RequestMapping("/test1")
    public String test( HttpServletRequest request,String productCode,String userId,double price)

    {
        PamsVo pamsVo = new PamsVo();
        pamsVo.setTate(new Date());
        pamsVo.setPrice(new BigDecimal(price));
        pamsVo.setUserId(userId);
        server1.sFunction1( productCode, userId, price,  pamsVo);
        return "ok";
    }
}
