package com.client.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author liuli
 * @date 2022/2/14
 */
public class PamsVo implements Serializable {

    private static final long serialVersionUID = -4852692530523389729L;

    String userId;

    Date tate;

    int times;

    BigDecimal price;

    List<String> productList;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getTate() {
        return tate;
    }

    public void setTate(Date tate) {
        this.tate = tate;
    }

    public int getTimes() {
        return times;
    }

    public void setTimes(int times) {
        this.times = times;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public List<String> getProductList() {
        return productList;
    }

    public void setProductList(List<String> productList) {
        this.productList = productList;
    }
}
