package com.client.service;



/**
 *  测试服务
 * @author liuli
 * @date 2022/2/9
 */
public interface Server2 {

    /***
     * 方法A
     * @param productCode
     * @param price
     */
    void sFunction2(String productCode, double price);

    /***
     * 满足条件 执行的方法B
     * @param productCode
     * @param price
     */
    void sFunction2GrayScale(String productCode, double price);
}
