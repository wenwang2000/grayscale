package com.client.service.impl;


import com.client.service.Server2;
import com.grayscale.annotation.GrayScaleFunction;
import org.springframework.stereotype.Service;

/**
 * @author liuli
 * @date 2022/2/9
 */
@Service
public class Server2Impl implements Server2 {

    @Override
    @GrayScaleFunction
    public void sFunction2(String productCode, double price) {
        System.out.println("sFunction2=不切");
    }

    @Override
    public void sFunction2GrayScale(String productCode, double price) {
        System.out.println("sFunction2GrayScale=切");
    }
}
