package com.client.service.impl;

import com.client.service.Server1;
import com.client.vo.PamsVo;
import com.grayscale.annotation.GrayScaleFunction;
import org.springframework.stereotype.Service;


/**
 * TestController中调用sFunction1，按照GrayScale配置规则在满足条件的情况下切换到sFunction1GrayScale
 * 这2个方法的形参必须一样
 * @author liuli
 * @date 2022/2/9
 */
@Service
public class Server1Impl implements Server1 {

    @Override
    /**
     * 如不指定 grayScaleMethod 默认：该方法名+GrayScale
     * @GrayScaleFunction(grayScaleMethod="sFunction1GrayScale")
     * */
    @GrayScaleFunction
    public void sFunction1(String productCode, String userId, double price, PamsVo pamsVo) {
        System.out.println("sFunction1=不切量");
    }

    @Override
    public void sFunction1GrayScale(String productCode, String userId, double price, PamsVo pamsVo){
        System.out.println("sFunction1GrayScale=切量");
    }

}
