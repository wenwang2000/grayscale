package com.client.service;


import com.client.vo.PamsVo;

/**
 * 测试服务
 * @author liuli
 * @date 2022/2/9
 */
public interface Server1 {

    /***
     * 方法A
     * @param productCode
     * @param userId
     * @param price
     * @param pamsVo
     */
     void sFunction1(String productCode, String userId, double price, PamsVo pamsVo);

    /***
     * 满足条件 执行的方法B
     * @param productCode
     * @param userId
     * @param price
     * @param pamsVo
     */
    void sFunction1GrayScale(String productCode, String userId, double price, PamsVo pamsVo);
}
